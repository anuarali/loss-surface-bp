import tensorflow as tf


class DenseLayer(tf.keras.Model):
    def __init__(self, layer_index, inner_size, n_channels):
        super(DenseLayer, self).__init__(name='dense__DenseLayer{0}'.format(layer_index))
        self.layer_number = inner_size
        self.bns = []
        self.convs = []
        self.relus = []
        self.bns3 = []
        self.convs3 = []
        self.relus3 = []
        self.dropouts = []
        self.dropouts2 = []
        for i in range(0, self.layer_number):
            self._one_layer(n_channels)
        self.concat = tf.keras.layers.concatenate

    def _one_layer(self, n_channels):
        self.bns.append(tf.keras.layers.BatchNormalization())
        self.relus.append(tf.keras.layers.ReLU())
        self.convs.append(tf.keras.layers.Conv2D(4 * n_channels, (1, 1), strides=1, padding='same'))

        self.bns3.append(tf.keras.layers.BatchNormalization())
        self.relus3.append(tf.keras.layers.ReLU())
        self.convs3.append(tf.keras.layers.Conv2D(n_channels, (3, 3), strides=1, padding='same'))

        self.dropouts.append(tf.keras.layers.Dropout(rate=0.25))
        self.dropouts2.append(tf.keras.layers.Dropout(rate=0.25))

    def call(self, inputs, training=None, mask=None):
        x = inputs
        for i in range(0, self.layer_number):
            y = self.bns[i](x, training=training)
            y = self.relus[i](y)
            y = self.convs[i](y)
            y = self.dropouts[i](y)

            y = self.bns3[i](y, training=training)
            y = self.relus3[i](y)
            y = self.convs3[i](y)
            y = self.dropouts[i](y)
            x = self.concat([x, y])
        return x


class TransitionLayer(tf.keras.Model):
    def __init__(self, layer_index, n_channels):
        super(TransitionLayer, self).__init__(name='dense__TransLayer{0}'.format(layer_index))
        self.bn = tf.keras.layers.BatchNormalization()
        self.relu = tf.keras.layers.ReLU()
        self.conv = tf.keras.layers.Conv2D(n_channels, (1, 1), padding='same')
        self.avgpool = tf.keras.layers.AveragePooling2D(2, strides=(2, 2))
        self.dropout = tf.keras.layers.Dropout(rate=0.25)
        #, noise_shape=(1, n_channels)

    def call(self, inputs, training=None, mask=None):
        x = self.bn(inputs, training=training)
        x = self.relu(x)
        x = self.conv(x)
        x = self.dropout(x)
        x = self.avgpool(x)
        return x


class DenseNet(tf.keras.Model):

    def __init__(self, input_shape, dense_channels=32, dense_size=[6, 12, 24, 16], output_classes=10):
        # 32 -> for 121 DenseNet
        super(DenseNet, self).__init__()
        # Initial layers
        self.input_layer = tf.keras.layers.Input(input_shape)
        self._create_initial_conv_layer(dense_channels)

        # 32 ->
        for i in range(0, 3):
            layer = DenseLayer(i, dense_size[i], dense_channels)
            setattr(self, 'denses_{0}'.format(i), layer)
            layert = TransitionLayer(i, (i + 2) * dense_channels)
            setattr(self, 'transes_{0}'.format(i), layert)

        self.dense4 = DenseLayer(4, dense_size[3], dense_channels)
        self._create_final_layers(output_classes)
        # img_input = tf.keras.layers.Input(shape=input_shape)
        # out = self.call(img_input)
        # self = tf.keras.Model(inputs=img_input, outputs=out)

    def _create_initial_conv_layer(self, dense_channels):
        self.zeropad1 = tf.keras.layers.ZeroPadding2D(padding=(3, 3), name='dense_CONV1_zero_padding0')
        self.conv1 = tf.keras.layers.Conv2D(dense_channels * 2, kernel_size=(7, 7), strides=(2, 2),
                                            name='dense_CONV1_Conv2D', padding='valid')

        self.bn1 = tf.keras.layers.BatchNormalization(name='dense_CONV1_BatchNorm')
        self.relu1 = tf.keras.layers.ReLU(name='dense_CONV1_ReLU')

        self.zeropad1_1 = tf.keras.layers.ZeroPadding2D(padding=(1, 1),
                                                        name='dense_CONV1_zero_padding1')
        self.maxPool1 = tf.keras.layers.MaxPool2D(pool_size=(3, 3), name='dense_CONV1_MaxPool2D',
                                                  strides=(2, 2), padding='valid')

    def _create_final_layers(self, output_classes):
        self.avgPool = tf.keras.layers.AveragePooling2D((2, 2), name='dense_FINAL_AvgPool2D')
        self.flatten = tf.keras.layers.Flatten()
        self.fc = tf.keras.layers.Dense(output_classes, use_bias=True, name='dense_FINAL_Dense_FC',
                                        kernel_initializer='he_normal')
        self.softmax = tf.keras.layers.Softmax(name='dense_FINAL_Softmax')
        self.out = self.call(self.input_layer)  # SOME TENSOR MAGIC

    def call(self, inputs, training=False, mask=None):
        # Initial layer
        x = self.zeropad1(inputs)
        # x = inputs
        x = self.conv1(x)
        # x = self.avgPool1(x)
        x = self.bn1(x, training=training)
        x = self.relu1(x)
        x = self.zeropad1_1(x)
        x = self.maxPool1(x)

        # Some bigger layers
        for i in range(0, 3):
            layer = getattr(self, 'denses_{0}'.format(i))
            x = layer(x, training=training)
            layert = getattr(self, 'transes_{0}'.format(i))
            x = layert(x, training=training)

        x = self.dense4(x, training=training)

        # Final layer
        x = self.avgPool(x)
        x = self.flatten(x)
        x = self.fc(x)
        x = self.softmax(x)
        return x
