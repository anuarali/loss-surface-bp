from threading import Thread
import copy
import time
import pandas as pd
import numpy as np
import csv
import pickle
import queue
import multiprocessing

GLOBAL_MAX_BOUND = 2
GLOBAL_DIV_NUM = 200
CPU_NUM = 2
ROW_START = 0
ROW_END = 20

q = queue.Queue()
global points
points = None
global cuda0
cuda0 = None

def compute_one_loss(model, origin, trajectories, datas, bound, num_of_division, criterion):
    global points
    while True:
        i = -1.0

        (i_int, i) = q.get()
        if i_int == -1:
            q.task_done()
            return
        j_int = 0

        if i_int < ROW_START or i_int >= ROW_END:
            for j in np.linspace(-bound, bound, num=num_of_division):
                points[i_int][j_int] = -1
                j_int += 1
            q.task_done()
            print('{0} row skipped'.format(i_int))
            continue
        print('{0} row started: {1}'.format(i_int, points[i_int]))
        for j in np.linspace(-bound, bound, num=num_of_division):
            parameter = origin + i * trajectories[0] + j * trajectories[1]
            # print('PARAMETERS have shape:{0}'.format(len(parameter)))

            # Set parameters to the model
            prev_size = 0
            t = 0
            #sd = model.state_dict()
            for var in model.parameters():
                # var = model.trainable_variables[t]
                # print('variable name:{0}'.format(var.name))
                shp = var.shape
                total = 1
                for s in shp:
                    total *= s

                new_size = prev_size + total

                values = parameter[prev_size: new_size]

                tens = np.reshape(values, var.shape)
                import torch
                #sd[var.name] = torch.Tensor(tens).to(cuda0)
                var.data = torch.Tensor(tens).to(cuda0)
                prev_size = new_size

            # Compute loss from parameters
            running_loss = 0
            for inputs, labels in datas:
                # get the inputs; data is a list of [inputs, labels]
                #inputs, labels = data
                inputs = inputs.to(cuda0)
                labels = labels.to(cuda0)
                # forward + backward + optimize
                outputs = model(inputs)
                loss = criterion(outputs, labels)
                # add statistics
                running_loss += loss.item()
                torch.cuda.empty_cache()
            # Add point's value
            points[i_int][j_int] = running_loss

            j_int += 1

            print('[{2}]status: {0}/{1}'.format(j_int, num_of_division, i_int))

        q.task_done()
        print('{0} row finished: {1}'.format(i_int, points[i_int]))
    return ()


def compute_mesh(model, trajectories, origin, trainloader, optimizer, loss, cuda, start, end):

    """
        Compute all points of mesh for the 3D graph

      !WARNING!
      For a large path this will take a long time
      !WARNING!

      -----------------------------------------------------------------
      Parameters:
          model (nn.Module):Model used for training
          trajectories (np.array):2 computed PCA components
          origin (np.array):Found minimum of the parameters
          data_x (np.array):Input data used in training
          data_y (np.array):Output data used in training
    """
    ################## Temporary variables ###############################################
    global ROW_START
    ROW_START = start
    global ROW_END
    ROW_END = end

    global cuda0
    cuda0 = cuda
    bound = GLOBAL_MAX_BOUND
    num_of_division = GLOBAL_DIV_NUM
    print('TOTAL CALCULATION SIZE')
    print(bound)
    print(num_of_division)
    ################## Temporary variables ###############################################
    datas = []
    for gh, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        #inputs = inputs.to(cuda0)
        #labels = labels.to(cuda0)
        datas.append((inputs, labels))
        
        # forward + backward + optimize
        #outputs = model(inputs)
        #loss = criterion(outputs, labels)
        # add statistics
        #running_loss += loss.item()

 
    global points
    points = np.zeros((num_of_division, num_of_division))
    # import tf.keras
    processes = []
    print('Initial points:{0}'.format(points))
    print('Bounds: Min{0}, Max{1}'.format(-GLOBAL_MAX_BOUND, GLOBAL_MAX_BOUND))
    print('Rows are in bound {0} to {1}'.format(ROW_START, ROW_END))
    print('bound:-{0}, {1}. Num of divisions:{2}'.format(bound, bound, num_of_division))
    print('Creating {0} processes...'.format(CPU_NUM))
    for i in range(0, CPU_NUM):
        arges = [model, origin, trajectories, datas, bound, num_of_division, loss]
        p = Thread(target=compute_one_loss, args=arges)
        p.start()
        processes.append(p)
    start = time.time()

    print('Creating Tasks....')
    i_s = np.linspace(-bound, bound, num=num_of_division)
    for ind in range(0, num_of_division):
        q.put((ind, i_s[ind]))

    print('Waiting for all processes to finish...')
    q.join()

    # Send end signal
    for i in range(0, CPU_NUM):
        q.put((-1, 0.0))
    q.join()
    print('Returning....')
    print('Time taken:{0}'.format(time.time() - start))
    ############ Return variables ###################
    res_points = np.array(points[:])
    # print(res_points)
    res_points = res_points.reshape((num_of_division, num_of_division))
    return res_points, bound, num_of_division
