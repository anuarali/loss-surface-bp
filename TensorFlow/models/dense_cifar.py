import warnings

warnings.filterwarnings('ignore')
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
#
import tensorflow as tf
from tensorflow.keras import models, layers
from tensorflow.keras.models import Model
from tensorflow.keras.layers import BatchNormalization, Activation, Flatten
from tensorflow.keras.optimizers import Adam


class DenseNetCifar(object):
    """
        Implementation of DenseNet for Cifar10
        Taken from:
        https://www.kaggle.com/asrsaiteja/densenet-tf-keras-on-cifar-10
    """

    def __init__(self, input_shape=None, dense_blocks=3, dense_layers=-1, growth_rate=12, num_classes=None,
                 dropout_rate=None, bottleneck=False, compression=1.0, weight_decay=1e-4, depth=40):

        # Parameters Check
        if num_classes == None:
            raise Exception(
                'Please define number of classes (e.g. num_classes=10). This is required to create .')

        if compression <= 0.0 or compression > 1.0:
            raise Exception('Compression have to be a value between 0.0 and 1.0.')

        if type(dense_layers) is list:
            if len(dense_layers) != dense_blocks:
                raise AssertionError('Number of dense blocks have to be same length to specified layers')
        elif dense_layers == -1:
            dense_layers = int((depth - 4) / 3)
            if bottleneck:
                dense_layers = int(dense_layers / 2)
            dense_layers = [dense_layers for _ in range(dense_blocks)]
        else:
            dense_layers = [dense_layers for _ in range(dense_blocks)]

        self.dense_blocks = dense_blocks
        self.dense_layers = dense_layers
        self.input_shape = input_shape
        self.growth_rate = growth_rate
        self.weight_decay = weight_decay
        self.dropout_rate = dropout_rate
        self.bottleneck = bottleneck
        self.compression = compression
        self.num_classes = num_classes

    def build_model(self):
        """
        Build the model
        Returns: tf Keras Model instance
        """
        if self.bottleneck:
            print('Creating DenseNet with Bottlenecks')
        else:
            print('Creating DenseNet without Bottlenecks')
        print('-' * 50)
        print('No.of. dense blocks: %s' % self.dense_blocks)
        print('Layers per dense block: %s' % self.dense_layers)
        print('-' * 50)

        # Input Layer
        img_input = layers.Input(shape=self.input_shape, name='img_input')
        nb_channels = self.growth_rate

        # Input-convolution layer
        x = layers.Conv2D(2 * self.growth_rate, (3, 3), padding='same', strides=(1, 1), name='input_conv',
                          kernel_regularizer=tf.keras.regularizers.l2(self.weight_decay))(img_input)

        # Building dense blocks
        for block in range(self.dense_blocks - 1):
            # Add dense_block
            x, nb_channels = self.dense_block(x, self.dense_layers[block], nb_channels, self.growth_rate,
                                              self.dropout_rate, self.bottleneck, self.weight_decay)

            # Add transition
            x = self.bn_relu_transition(x, nb_channels, self.dropout_rate,
                                        self.compression, self.weight_decay)  # 1 conv layer
            nb_channels = int(nb_channels * self.compression)

        # Add last dense block without transition but with only global average pooling
        x, nb_channels = self.dense_block(x, self.dense_layers[-1], nb_channels,
                                          self.growth_rate, self.dropout_rate, self.weight_decay)

        # prediction of class happens here
        x = layers.BatchNormalization(name='prediction_bn')(x)
        x = layers.Activation('relu', name='prediction_relu', )(x)
        x = layers.GlobalAveragePooling2D(name='prediction_pool', )(x)
        prediction = layers.Dense(self.num_classes, name='prediction_dense', activation='softmax')(x)

        return tf.keras.Model(inputs=img_input, outputs=prediction, name='DenseNetCifar')

    def dense_block(self, x, num_layers, nb_channels, growth_rate, dropout_rate=None, bottleneck=False,
                    weight_decay=1e-4):
        """
        Creates a dense block and concatenates inputs
        """

        for i in range(num_layers):
            cb = self.bn_relu_convolution(x, growth_rate, dropout_rate,
                                          bottleneck)  # 1 conv if bottleneck = 0 else 2 conv if bottleneck = 1
            nb_channels += growth_rate
            x = layers.concatenate([cb, x])
        return x, nb_channels

    def bn_relu_convolution(self, x, nb_channels, dropout_rate=None, bottleneck=False, weight_decay=1e-4):
        """
        Creates a convolution layers consisting of BN-ReLU-Conv.
        Optional: bottleneck, dropout

        """
        # Bottleneck
        if bottleneck:
            bottleneckWidth = 4
            x = layers.BatchNormalization()(x)
            x = layers.Activation('relu')(x)
            x = layers.Conv2D(nb_channels * bottleneckWidth, (1, 1),
                              kernel_regularizer=tf.keras.regularizers.l2(weight_decay))(x)
            # Dropout
            if dropout_rate:
                x = layers.Dropout(dropout_rate)(x)

        # BN-ReLU-Conv
        x = layers.BatchNormalization()(x)
        x = layers.Activation('relu')(x)
        x = layers.Conv2D(nb_channels, (3, 3), padding='same')(x)

        # Dropout
        if dropout_rate:
            x = layers.Dropout(dropout_rate)(x)

        return x

    def bn_relu_transition(self, x, nb_channels, dropout_rate=None, compression=1.0, weight_decay=1e-4):
        """
        Creates a transition layer between dense blocks as transition, which do convolution and pooling.
        Works as downsampling.
        """

        x = layers.BatchNormalization()(x)
        x = layers.Activation('relu', )(x)
        x = layers.Convolution2D(int(nb_channels * compression), (1, 1), padding='same',
                                 kernel_regularizer=tf.keras.regularizers.l2(weight_decay))(x)

        # Adding dropout
        if dropout_rate:
            x = layers.Dropout(dropout_rate)(x)

        x = layers.AveragePooling2D((2, 2), strides=(2, 2))(x)
        return x
