import numpy as np
import torch.nn as nn


def filter_normalization(direction: np.ndarray, layer_variables: np.ndarray):
    rand_final = []
    for d, w in zip(direction, layer_variables):
        d = d * (1 / np.linalg.norm(d) + 1e-10)  # |d| = 1
        w_size = np.linalg.norm(w)  # get |w|
        d = d * w_size  # |d| = |w|

        d = np.reshape(d, -1)
        rand_final = np.concatenate((rand_final, d), axis=0)

    # print('rand final:{0}'.format(rand_final))
    # print('#############################################################')
    return rand_final


def get_random_trajectories(model: nn.Module):
    rand_dir = []
    orig_dir = []
    for name, param in model.named_parameters():
        if param.requires_grad:
            # print('Name:{0}, Dim:{1}'.format(name, param.dim()))
            a = 1
        else:
            print('Non grad')
            continue
        param_np = param.cpu().detach().numpy();
        shape = param.shape
        d = None
        if param.dim() <= 1:
            d = np.zeros(shape=shape)
            d = np.reshape(d, -1)
            # print('########## zeroed ###########')
        else:
            rd = np.random.normal(size=shape)
            d = filter_normalization(rd, param_np)
        rand_dir = np.concatenate((rand_dir, d), axis=0)

        param_f = np.reshape(param_np, -1)
        orig_dir = np.concatenate((orig_dir, param_f), axis=0)
    return rand_dir, orig_dir
