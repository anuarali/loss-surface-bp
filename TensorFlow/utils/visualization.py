import math
import threading
import time

import numpy as np
import pickle

# from matplotlib import cm
import numpy as np
import vtk

from matplotlib import pyplot as plt, cm, transforms
import matplotlib;
# matplotlib.use("TkAgg")   # Uncomment to use interactive matplotlib
from matplotlib.colors import LightSource, ListedColormap
from matplotlib import cm
from enum import Enum
import pyvista as pv
import numpy as np


class Visualization:
    class Method(Enum):
        VTK_image = 0
        VTK_video = 1
        MATPLOT_3D = 2
        CONTOUR_2D = 3
        HEATMAP_2D = 4

    @staticmethod
    def print_previsualized():
        print('------------------------------------------------------')
        print('         All pre-visualized models                    ')
        all_models = ['resnet56_noskip', 'resnet56', 'resnet110', 'resnet110_noskip', 'resnet50', 'resnet50_noskip',
                      'taaf_3x6000_pca', 'taaf_3x6000_filnorm', 'taaf_3x9000_pca', 'taaf_3x9000_filnorm',
                      'dense_3x6000_pca', 'dense_3x6000_filnorm', 'dense_3x9000_pca', 'dense_3x9000_filnorm',
                      'densenet_cifar_filnorm', 'densenet_cifar_pca', 'densenet_imagenet', 'resnet50_imagenet']
        for i, model in enumerate(all_models):
            print('{0:2d}) {1:30s}                    '.format(i, model))

    @staticmethod
    def matplotlib_3d_plot(points: np.array, xy_bound: float, resolution: int,
                           rotate=False):
        """
        Visualise a 3D graph of the Loss Surface from points given

        First points of a given array corresponds to the [-xy_bound, -xy_bound] coordinate
        Last point of first inner array corresponds to the [xy_bound, -xy_bound] coordinate
        Example: [[(-1,-1), (0,-1), (1,-1)], [(-1,0), ...], ...] for bound=1, step=1

        :param points: (np.array) 2D array of Loss Surface values
        :param xy_bound: (float) Positive float with the halfbound of the XY plane, e.g. (-b, b)
        :param resolution: (int) Number of divisions in [-xy_bound, -xy_bound] interval
        :param rotate: (boolean) True if 3D plot needs to be rotate ~180 degrees on Z axis (Optional)

        :raises ValueError: if array length is not [resolution x resolution]
        """

        if len(points) != resolution:
            raise ValueError(
                "Given points' dimension is wrong! ({ln} : {num}) Either set your own dimension or reshape it!".format(
                    ln=len(points), num=resolution))
        else:
            print('############### VISUALIZING {0}x{0} ################'.format(resolution))

        # Create a figure inside our plot
        fig = plt.figure(figsize=(5, 5))
        ax = plt.subplot(111, projection='3d')
        print('ax.azim {}'.format(ax.azim))
        print('ax.elev {}'.format(ax.elev))

        if rotate:
            ax.view_init(azim=-190, elev=25)
        # Remove gray panes and axis grid(background axis)
        ax.xaxis.pane.fill = False
        ax.xaxis.pane.set_edgecolor('white')
        ax.yaxis.pane.fill = False
        ax.yaxis.pane.set_edgecolor('white')
        ax.zaxis.pane.fill = False
        ax.zaxis.pane.set_edgecolor('white')
        ax.grid(False)

        # Create meshgrid from values
        X, Y = np.meshgrid(np.linspace(-xy_bound, xy_bound, num=resolution),
                           np.linspace(-xy_bound, xy_bound, num=resolution))

        xnew = np.linspace(-1, 1, 300)
        ynew = np.linspace(-1, 1, 300)

        light = LightSource(azdeg=315, altdeg=1)

        illuminated_surface = light.shade(points, cmap=plt.cm.coolwarm,
                                          blend_mode='soft')
        plot = ax.plot_surface(X=X, Y=Y, Z=points, cmap=cm.coolwarm, antialiased=False,
                               # facecolors=illuminated_surface,
                               shade=False)

        plt.show()

    @staticmethod
    def load_mesh(filename):
        """
        Loads a pickle-dump file in format:
             (point, xy_bound, num)

        :param filename: path to the pickle-dump file
        :return: (point, xy_bound, num)
        """
        fl = open(filename, 'rb')
        res = pickle.load(fl)
        # points = res['points']
        try:
            points = res[0]
            w = len(points)
            h = len(points[0])
            print('Points have: {0}x{1}'.format(w, h))
            # for i in range(0, 51):
            # print('Points[{1}]:{0}'.format(points[i], i))
            # xy_b = res['xy_b']
            xy_b = res[1]
            # num = res['num']
            num = res[2]
            print('Points have: {0}'.format(points.shape))
            # print('Points:{0}'.format(points))
            return points, xy_b, num
        except KeyError:
            points = res['points']
            w = len(points)
            h = len(points[0])
            print('Points have: {0}x{1}'.format(w, h))
            xy_b = res['xy_b']
            num = res['num']
            print('Points have: {0}'.format(points.shape))
            # print('Points:{0}'.format(points))
            return points, xy_b, num

        return points, xy_b, num

    @staticmethod
    def heatmap(points: np.array):
        """
        Visualizes the heatmap of the loss surface

        :param points: 2D array of points of loss surface
        """
        # 'hot'
        # 'viridis'
        plt.imshow(points, cmap='viridis', interpolation='nearest')
        plt.colorbar()
        plt.show()

    @staticmethod
    def contour(points: np.array, name):
        """
        Visualizes the contour of the loss surface

        :param points: 2D array of points of loss surface
        :param name: name of the model to be printed on plot
        """
        fig, ax = plt.subplots()
        CS = ax.contour(np.flip(points, 0))  # Flip because of numpy library format!
        ax.clabel(CS, inline=True, fontsize=10)
        ax.set_title(name)
        plt.show()

    @staticmethod
    def rotate_via_numpy(xy, radians):
        """
        Support function to rotate a 3D vectors along z axis

        :param xy: (vector, vector), in format of arrays
        :param radians: angle to rotate in radians
        :return: (vector, vector), but rotated
        """
        x, y = xy
        c, s = np.cos(radians), np.sin(radians)
        j = np.matrix([[c, s], [-s, c]])
        m = np.dot(j, [x, y])

        return float(m.T[0]), float(m.T[1])

    @staticmethod
    def plotVTK(all_points, scale, resolution, save_file='tmp.png', xy_scale=1.0, is_video=True):
        """
        Compact function to visualize a VTK format plot. Uses PyVista library

        :param all_points: 2D array of points
        :param scale: size of X and Y axes of the plot.
        :param resolution: number of points per axis in 2D array
        :param save_file: path to screentshot file to be saved (Works only for is_video=False)
        :param xy_scale: scaling of a previous <scale> parameter if plot is too big
        :param is_video: if True -> Video of rotating plot, else -> Screenshot of rotated plot
        """
        # Create a spatial reference
        inc = 2 * scale / (resolution - 1)
        x = np.arange(0, resolution) * inc * xy_scale
        y = np.arange(0, resolution) * inc * xy_scale
        xx, yy, _ = np.meshgrid(x, y, [0])
        zz = np.expand_dims(all_points, axis=2)

        # Make a PyVista/VTK mesh
        surface = pv.StructuredGrid(xx, yy, zz)
        surface['height'] = surface.points[:, 2]

        boring_cmap = plt.cm.get_cmap("coolwarm")
        # surface.plot(show_edges=False, show_grid=True, notebook=False, scalars='height', cmap=boring_cmap)
        if is_video:
            print('VIDEO')
            Visualization.plotVTK_video(surface, boring_cmap)
        else:
            print('Screenshot')
            Visualization.plotVTK_screenshot(surface, boring_cmap, name=save_file)

    @staticmethod
    def plotVTK_video(surface, heatmap):
        """
        Visualize a VTK as video of rotation
        :param surface: VTK object
        :param heatmap: Heatmap of VTK 2D surface, for coloring
        """
        plotter = pv.Plotter()

        # plotter.add_checkbox_button_widget(test_callback, value=False)
        plotter.add_mesh(surface, cmap=heatmap)
        mean = surface.points.mean(0)
        surface.points -= [mean[0], mean[1], 25]
        cpos = plotter.show()

        plotter = pv.Plotter()  # off_screen=True

        plotter.add_mesh(surface, cmap=heatmap)

        plotter.camera_position = cpos
        from scipy.spatial.transform import Rotation as R
        plotter.open_movie('Rotate_test.mp4')
        plotter.show(auto_close=False)
        plotter.write_frame()
        for i in range(360):
            cpos = plotter.camera_position
            # plotter.clear()
            # plotter.add_mesh(surface, cmap=heatmap)

            rotated = Visualization.rotate_via_numpy((cpos[0][0], cpos[0][1]), np.radians(1))

            plotter.set_position([rotated[0], rotated[1], cpos[0][2]])

            plotter.add_text("Iteration: {:d}".format(i), name='time-label')  #
            plotter.write_frame()  # Write this frame
        plotter.close()
        exit(0)

    @staticmethod
    def plotVTK_screenshot(surface, heatmap, name):
        """
            Visualize a VTK as screenshot after interactive rotation
            :param surface: VTK object
            :param heatmap: Heatmap of VTK 2D surface, for coloring
            :param name: file to save a screenshot
        """
        plotter = pv.Plotter()

        plotter.add_mesh(surface, cmap=heatmap)
        mean = surface.points.mean(0)
        surface.points -= [mean[0], mean[1], 25]
        cpos = plotter.show()

        plotter = pv.Plotter(off_screen=True)
        plotter.add_mesh(surface, cmap=heatmap)
        plotter.camera_position = cpos
        # plotter.screenshot('test.png')
        plotter.show(screenshot=name)
        exit(0)

    @staticmethod
    def getPretrained(name):
        """
        Support function to get a pre-visualized model file.
        Save file format for:
            total_size = 15, row_size=5
            save_file0, save_file5, save_file10

        :param name: Name of pre-visualized model
        :return: (file, total_size, row_size)
        :raises ValueError: if non-existing name sent
        """

        test = None

        # ResNet56 noskip
        if name == 'resnet56_noskip':
            test = 'data/Visualizations/ResNet_56_noskip_bound1_resolution_200_tensorflow'
            total_size = 200
            thread_size_rows = 20
        # ResNet56
        if name == 'resnet56':
            test = 'data/Visualizations/ResNet_56_bound1_resolution_200_tensorflow'
            total_size = 200
            thread_size_rows = 20

        # TAAF, 3x9000, PCA, 200, 20
        if name == 'taaf_3x9000_pca':
            # test = 'data/TAAF_3x9000_PCA/result_pca{0}.txt'
            test = 'data/Visualizations/TAAF_3x9000_pca'
            total_size = 200
            thread_size_rows = 10
        # TAAF, 3x9000, filnorm, 200, 20
        if name == 'taaf_3x9000_filnorm':
            # test = 'data/TAAF_3x9000_rand/result_rand_3x9000_{0}.txt'
            test = 'data/Visualizations/TAAF_3x9000_filnorm'
            total_size = 200
            thread_size_rows = 10

        # TAAF, 3x6000, PCA, 480, 20
        if name == 'taaf_3x6000_pca':
            # test = 'data/TAAF_3x6000_PCA/result_pca{0}.txt'
            test = 'data/Visualizations/TAAF_3x6000_pca'
            total_size = 480
            thread_size_rows = 20
        # TAAF, 3x6000, filnorm, 400, 20
        if name == 'taaf_3x6000_filnorm':
            # test = 'data/TAAF_3x6000_rand/result_rand_3x6000_{0}.txt'
            test = 'data/Visualizations/TAAF_3x6000_filnorm'
            total_size = 400
            thread_size_rows = 20

        # Dense, 3x6000, PCA, 300, 15
        if name == 'dense_3x6000_pca':
            # test = 'data/TAAF_3x6000_PCA_lin/result_PCA_3x6000_{0}.txt'
            test = 'data/Visualizations/Dense_3x6000_pca'
            total_size = 300
            thread_size_rows = 15
        # Dense, 3x6000, filnorm, 300, 15
        if name == 'dense_3x6000_filnorm':
            # test = 'data/TAAF_3x6000_lin_rand/result_rand_3x6000_{0}.txt'
            test = 'data/Visualizations/Dense_3x6000_filnorm'
            total_size = 300
            thread_size_rows = 15

        # Dense, 3x9000, PCA
        if name == 'dense_3x9000_pca':
            # test = 'data/TAAF_3x9000_PCA_lin/result_rand_3x6000_{0}.txt'
            test = 'data/Visualizations/Dense_3x9000_pca'
            total_size = 300
            thread_size_rows = 15

        # Dense, 3x9000, filnorm, 200, 20
        if name == 'dense_3x9000_filnorm':
            # test = 'data/TAAF_3x9000_lin_rand/result_rand_3x9000l_{0}.txt'
            test = 'data/Visualizations/Dense_3x9000_filnorm'
            total_size = 300
            thread_size_rows = 15

        # ResNet50, noskip, filnorm
        if name == 'resnet50_noskip':
            # test = 'data/ResNet/result_res50no_{0}.txt'
            test = 'data/Visualizations/ResNet50_noskip'
            total_size = 300
            thread_size_rows = 15
        # ResNet50, filnorm
        if name == 'resnet50':
            # test = 'data/ResNet/result_res50_{0}.txt'
            test = 'data/Visualizations/ResNet50'
            total_size = 300
            thread_size_rows = 15
        # ResNet50, PCA
        if False:
            name == 'resnet50_pca'
            test = 'data/ResNet/result_res50{0}.txt'
            total_size = 200
            thread_size_rows = 20

        # ResNet50, noskip
        if False:
            test = 'data/ResNet/result_resno50_{0}.txt'
            total_size = 200
            thread_size_rows = 20
        if name == 'resnet50_old':
            test = 'data/Visualizations/(MSE)ResNet_50_bound1_resolution_200_tensorflow'
            # test = 'data/result_index{0}.txt'
            total_size = 200
            thread_size_rows = 200
        if name == 'resnet50_noskip_old':
            test = 'data/Visualizations/(MSE)ResNet_50_noskip_bound1_resolution_200_tensorflow'
            total_size = 200
            thread_size_rows = 10
        # ResNet110,
        if name == 'resnet110_old':
            test = 'data/Visualizations/ResNet_110_bound1_resolution_200_tensorflow'
            total_size = 200
            thread_size_rows = 200
        # ResNet110, noskip
        if name == 'resnet110_noskip':
            test = 'data/Visualizations/ResNet_110_noskip_bound3_resolution_300_tensorflow'
            total_size = 300
            thread_size_rows = 300

        # ResNet110, 300, bound = 3
        if name == 'resnet110':
            test = 'data/ResNet/noskip_{0}_{1}'
            total_size = 300
            thread_size_rows = 10

        if name == 'densenet_cifar_filnorm':
            # test = 'data/DenseNet_cifar/result{0}.txt'
            test = 'data/Visualizations/DenseNet_cifar_filnorm'
            total_size = 200
            thread_size_rows = 10

        if name == 'densenet_cifar_pca':
            # test = 'data/DenseNet_cifar_pca/result{0}.txt'
            test = 'data/Visualizations/DenseNet_cifar_pca'
            total_size = 200
            thread_size_rows = 10

        # DenseNet ImageNet
        if name == 'densenet_imagenet':
            test = 'data/Visualizations/DenseNet_imagenet'
            total_size = 60
            thread_size_rows = 3

        # ResNet110 Noskip Test
        if name == 'resnet110_noskip_test':
            # test = 'data/noskip_{0}_{1}'
            test = 'data/Visualizations/ResNet110_noskip_TEST'
            total_size = 300
            thread_size_rows = 10

        # ResNet110 Test
        if name == 'resnet110_test':
            # test = 'data/noskip_{0}_{1}'
            test = 'data/Visualizations/ResNet110_TEST'
            total_size = 300
            thread_size_rows = 10

        # ResNet50 ImageNet
        if name == 'resnet50_imagenet':
            # test = 'data/res50_rand2_{0}.txt'
            test = 'data/Visualizations/ResNet50_Test_log'
            total_size = 60
            thread_size_rows = 3

        if test is None:
            raise ValueError('Wrong pre-visualized model name used!')

        return test, total_size, thread_size_rows

    @staticmethod
    def visualize_plot(model_name: str, vis_method=Method.VTK_video,
                       log_z=False, double_log_z=False, exp_z=False, divide_plot=1, xy_scale=1,
                       save_file=None, max_threshold=math.exp(50), rotate_3d=False):
        """
        Universal function for all visualization methods implemented. Works on pre-visualized models only.

        :param model_name: Pre-trained model name
        :param vis_method: Method of visualization. Used as enum of <Method> class:
        {CONTOUR_2D, HEATMAP_2D, MATPLOT_3D, VTK_image, Method.VTK_video}
        :param log_z: True if loss values need to be logarithmed -> log(z)
        :param double_log_z: True if loss values need to be logarithmed 2 time -> log(log(z)) (WORKS ONLY WITH log_z=True)
        :param exp_z: True if loss values need to be exponentiated -> exp(z)
        :param divide_plot: Integer > 1, cuts a plot, e.g. 2 -> 0.5, 0.5
        :param xy_scale: Scaling of X,Y axes if VTK method is used
        :param max_threshold: Threshold of values. All larger values are clamped to this.
        :param save_file: Path to the save file of plot(+ all previous transforms, None = No saving)
        :param rotate_3d: True if Matplotlib_3D plot needs to be rotated ~180 degree along Z axis
        """
        test, total_size, thread_size_rows = Visualization.getPretrained(model_name)

        results = []

        for i in range(0, total_size, thread_size_rows):
            results.append(test.format(i, i + thread_size_rows))

        all_points = None
        xy_b = None
        num = None

        for ind in range(0, len(results)):
            r = results[ind]
            if r is None:
                continue
            try:
                points1, xy_b1, num1 = Visualization.load_mesh(r)
            except:
                print('skipping {0}'.format(r))
                continue
            if all_points is None:
                all_points = np.full((num1, num1), -1.0)
                print('Main width:{0}x{0}'.format(num1))
                num = num1
                xy_b = xy_b1
            print('Bound:-{0}x{0}'.format(xy_b1))
            w_max = min([(ind + 1) * thread_size_rows, num1])
            print('from {0} to {1}'.format(ind * thread_size_rows, w_max))
            for w in range(ind * thread_size_rows, w_max):
                if points1[w][1] == -1:
                    print('############ Row {0} is bad! ############'.format(w))
                for h in range(0, num1):
                    if not math.isfinite(points1[w][h]):
                        points1[w][h] = max_threshold
                        print('INF')
                    all_points[w][h] = points1[w][h]
                    if all_points[w][h] > max_threshold:
                        all_points[w][h] = max_threshold

        for w in range(0, num):
            # all_points[w] = all_points[w] / norm
            for h in range(0, num):
                if all_points[w][h] > 0:
                    # math.log(all_points[w][h]) - math.log(1 - p)
                    p = all_points[w][h]
                    # all_points[w][h] = math.e ** p
                    if log_z:
                        all_points[w][h] = np.log(p)
                        if double_log_z:
                            p = all_points[w][h]
                            if p > 0.0:
                                all_points[w][h] = np.log(p)
                    elif exp_z:
                        all_points[w][h] = np.exp(p)
                    # np.log
                    # if all_points[w][h] > 10:
                    # all_points[w][h] = 10

        # Cut needed part
        one_section = num // (2 * divide_plot)
        start = one_section * (divide_plot - 1)
        end = one_section * (divide_plot + 1)
        all_points = all_points[start:end, :]
        all_points = all_points[:, start:end]
        num = len(all_points)
        print(all_points.shape)
        print(num)

        if vis_method == Visualization.Method.HEATMAP_2D:
            Visualization.heatmap(np.array(all_points))
        elif vis_method == Visualization.Method.CONTOUR_2D:
            Visualization.contour(np.array(all_points), model_name)
        elif vis_method == Visualization.Method.VTK_image:
            Visualization.plotVTK(all_points, xy_b, num, xy_scale=xy_scale, is_video=False)
        elif vis_method == Visualization.Method.VTK_video:
            Visualization.plotVTK(all_points, xy_b, num, xy_scale=xy_scale, is_video=True)
        elif vis_method == Visualization.Method.MATPLOT_3D:
            Visualization.matplotlib_3d_plot(np.array(all_points), xy_bound=xy_b, resolution=num,
                                             rotate=rotate_3d)
        else:
            print('ERROR: Unknown type received!')
            return

        if type(save_file) != str or save_file == '':
            print('NOTE: Savefile should be non-empty string!')
            exit(0)
            return

        f = open(
            save_file, 'wb')
        final_result = [all_points, xy_b, num]
        pickle.dump(final_result, f)
        return

    @staticmethod
    def visualizedTraining(filename, epoch_num=200):
        """
        Visualize a training process in form of train loss/accuracy + validation loss/accuracy

        :param filename: path to the pickle-dumped file with tensorflow-keras history dictionary
        :param epoch_num: number of epochs trained
        """
        hist_path = str(filename)
        x = range(0, epoch_num)

        f_hist = open(hist_path, 'rb')
        hist = pickle.load(f_hist)
        for key in hist.keys():
            print(key)

        losses = hist['loss']
        val_losses = hist['val_loss']
        try:
            accs = hist['accuracy']
            val_accs = hist['val_accuracy']
        except KeyError:
            accs = hist['acc']
            val_accs = hist['val_acc']

        print(losses[-1])
        print(val_losses[-1])

        fig = plt.figure()
        host = fig.add_subplot(111)

        par1 = host.twinx()

        host.set_xlim(0, max(x) + 1)
        host.set_ylim(0, 10)
        par1.set_ylim(0, 1.25)

        host.set_xlabel("Epoch")
        host.set_ylabel("Loss")

        color1 = 'r'
        color2 = 'orange'
        color3 = 'g'
        color4 = 'b'

        p1, = host.plot(x, losses, color=color1, label="Loss(Average over batch)", linewidth=2)
        p2, = par1.plot(x, accs, color=color2, label="Accuracy", linewidth=2)
        p3, = host.plot(x, val_losses, color=color3, label="Validation Loss", linewidth=2)
        p4, = par1.plot(x, val_accs, color=color4, label="Validation Acc",
                        linewidth=2)

        lns = [p1, p2, p3, p4]

        host.legend(handles=lns, loc='center right')

        host.yaxis.label.set_color(p1.get_color())
        par1.yaxis.label.set_color(p2.get_color())

        plt.show()
