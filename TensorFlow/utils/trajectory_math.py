import tensorflow as tf
import numpy as np
import copy
from sklearn.decomposition import PCA


def filter_normalization(direction: np.ndarray, layer_variables: np.ndarray):
    """
    Normalize random trajectory with the filter normalization

    FOR 1 FILTER ONLY!
    E.G.  (16,3,3) = 16 filters of (3,3)
    E.G (16,3,3,3) = 16 filters of (3,3,3)

    :param direction: Random trajectory (np.ndarray) generated with Gaussian normal distribution for 1 filter
    :param layer_variables: Original filter weights (np.ndarray)
    :return: Normalized numpy array(np.ndarray) of trajectory for the given filter
    """
    rand_final = []
    for d, w in zip(direction, layer_variables):
        d = d * (1 / np.linalg.norm(d) + 1e-10)  # |d| = 1
        w_size = np.linalg.norm(w)  # get |w|
        d = d * w_size  # |d| = |w|
        d = np.reshape(d, -1)
        rand_final = np.concatenate((rand_final, d), axis=0)

    return rand_final


def get_random_filnorm_trajectory(model):
    """
    Generate 1 random trajectory with filter normalization.

    !Should be used on fully trained model with weights calculated!
    (Especially for Batch normalization layers and their statistics)

    :param model: Compiled model(tf.keras.Model) with 1 epoch fit(for Batch norm initialization)
    :return: numpy array of 1 filter-normalized random trajectory
    """
    rand_dir = []
    orig_dir = []
    for param in model.trainable_variables:
        # print('Name:{0}, Dim:{1}'.format(param.name, param.shape))
        param_np = param.numpy();
        shape = param.shape
        d = None
        if param_np.ndim <= 1:
            d = np.zeros(shape=shape)
            d = np.reshape(d, -1)
            # print('########## zeroed ###########')
        else:
            rd = np.random.normal(size=shape)
            d = filter_normalization(rd, param_np)
        rand_dir = np.concatenate((rand_dir, d), axis=0)

        param_f = np.reshape(param_np, -1)
        orig_dir = np.concatenate((orig_dir, param_f), axis=0)
    print('Final dimensions:')
    print(rand_dir.shape)
    return rand_dir, orig_dir


def model_PCA(model, checkpoint_path, epoch_number=200, step=5):
    """
    Complete calculation of PCA trajectories from model and saved checkpoints

    :param model: Compiled tf.keras.models.Model
    :param checkpoint_path: Complete path to the file saved from save_weights()
    :param epoch_number: Positive number of the trained epochs
    :param step: Positive number of period used to save the parameters
    :return: Pair of trajectories from PCA
    """

    checks = load_checkpoints(checkpoint_path, model, epoch_number, step)

    (transformedPath, trajectories, final_node) = PCA_from_path(checks)
    traj1 = trajectories[0]
    traj2 = trajectories[1]
    #traj1 = normalize_PCA(model, traj1)
    #traj2 = normalize_PCA(model, traj2)

    trajectories = [traj1, traj2]

    return trajectories


def PCA_from_path(check_params: np.array):
    """
    Computing a PCA trajectory components from a given path of checkpoints
    Last element of path, e.g. found minimum, is considered as an origin

    :param check_params: The array of a parameters
    :return: transformedPath (np.array): Transformed 2D coordinates of an input path
            trajectories (np.array): 2 PCA components, e.g. base of an 2D subspace
            final_node (np.array): Origin, e.g. last element of an array
    """
    # Copy an input array
    temp_params = copy.deepcopy(check_params)

    # Copy found minimum, e.g. last element
    final_node = copy.copy(temp_params[len(temp_params) - 1])

    # Make found minimum(final params) as an origin
    for i in range(0, len(temp_params)):
        temp_params[i] = np.subtract(temp_params[i], final_node)

    print('Input values have a shape:', end='')
    print(len(temp_params), end='')
    print(', ', end='')
    print(len(temp_params[0]))

    # Standardize the data (so that we get <0, 1> numbers)
    # x = StandardScaler().fit_transform(temp_params)

    # Compute PCA components
    pca = PCA(n_components=2)
    transformedPath = pca.fit_transform(temp_params)
    for vec in transformedPath:
        print(vec.shape)
    trajectories = pca.components_

    # Return PCA components
    return transformedPath, trajectories, final_node


def load_checkpoints(path_to_checks: str, model, epoch_number: int, step: int):
    """
    Load saved checkpoints from a formatted path:
        '/path/to/checkpoints/name{epoch:XXX}name'
        where {epoch} is the epoch of the saved parameters
        in <0, epoch_number> range with (step) size


        Example:
        path_to_checks = 'checkpoints/cp-{epoch:04d}.ckpt'
        epoch_number = 600
        step = 4

        so, files should look like:
        'checkpoints/cp-0000.ckpt'
        'checkpoints/cp-0004.ckpt'
        'checkpoints/cp-0008.ckpt'
        ...

    :param path_to_checks: Complete path to the file saved from save_weights()
    :param model: Compiled model with 1 epoch fit(for Batch norm initialization)
    :param epoch_number: Positive number of the trained epochs
    :param step: Positive number of period used to save the parameters
    :return: Parameters of model in form of array
    """
    path_params = []

    for i in range(step, epoch_number + 1, step):
        checkpoint_path = path_to_checks.format(epoch=i)
        model.load_weights(checkpoint_path)  # Load params to model

        weights = tf.zeros(0)
        for var in model.trainable_weights:  # Get them in 1D array
            values_1d = tf.reshape(var.read_value(), [-1])
            weights = tf.concat([weights, values_1d], 0)

        path_params.append(weights)  # Add them as array

        if i % (5 * step) == 0:
            print('Checkpoint {0}, number of parameters: '.format(i), end='')
            print(weights.shape)
    return path_params


def normalize_PCA(model, traj):
    rand_dir = []
    orig_dir = []
    traj_f = traj.flatten()
    curr_size = 0
    for param in model.trainable_variables:
        # print('Name:{0}, Dim:{1}'.format(param.name, param.shape))
        param_np = param.numpy();
        shape = param.shape
        total_size = 1
        for a in shape:
            total_size *= a
        prev_size = curr_size
        curr_size += total_size
        print('current:{0}-{1}'.format(prev_size, curr_size))
        d = None
        if param_np.ndim <= 1:
            d = np.zeros(shape=shape)
            d = np.reshape(d, -1)
            # print('########## zeroed ###########')
        else:
            # rd = np.random.normal(size=shape)
            rd = traj_f[prev_size:curr_size]
            rd = rd.reshape(shape)
            d = filter_normalization(rd, param_np)
        rand_dir = np.concatenate((rand_dir, d), axis=0)

        param_f = np.reshape(param_np, -1)
        orig_dir = np.concatenate((orig_dir, param_f), axis=0)
    print('Final dimensions:')
    print(rand_dir.shape)
    return rand_dir
