from threading import Thread
import copy
import time
import numpy as np
import tensorflow as tf
import csv
import pickle
import queue
import multiprocessing
from tensorflow.keras.layers import Input, Dense, Activation, Dropout
from tensorflow.keras.optimizers import Nadam

GLOBAL_MAX_BOUND = 1
GLOBAL_DIV_NUM = 10
CPU_NUM = 1
ROW_START = 0
ROW_END = 10

q = queue.Queue()

points = None


def compute_one_loss(model, origin, trajectories, data_x, data_y, bound, num_of_division, optimizer, loss):
    while True:
        i = -1.0

        (i_int, i) = q.get()
        if i_int == -1:
            q.task_done()
            return
        j_int = 0

        if i_int < ROW_START or i_int >= ROW_END:
            for j in np.linspace(-bound, bound, num=num_of_division):
                points[i_int][j_int] = -1
                j_int += 1
            q.task_done()
            print('{0} row skipped'.format(i_int))
            continue

        for j in np.linspace(-bound, bound, num=num_of_division):
            # Calculate change
            parameter = i * trajectories[0] + j * trajectories[1]

            # Set initial(0,0) parameters to the model
            model.load_weights(origin)

            # Assign change to variables
            prev_size = 0
            for t in range(0, len(model.trainable_variables)):
                var = model.trainable_variables[t]
                shp = var.shape.as_list()
                total = 1
                for s in shp:
                    total *= s

                new_size = prev_size + total
                values = parameter[prev_size: new_size]
                tens = np.reshape(values, var.shape)
                model.trainable_variables[t].assign_add(tens)
                prev_size = new_size

            # Compute loss from data TODO !Batch_size change!
            loss_val = model.evaluate(data_x, data_y, batch_size=128, verbose=0)

            # We are only interested in loss value (return is [loss, acc, etc])
            if isinstance(loss_val, (list, tuple, np.ndarray)):
                loss_val = loss_val[0]

            # Add point's value
            points[i_int][j_int] = loss_val

            j_int += 1
            print('[{2}]status: {0}/{1}. Value:{3}. Coordinate:{4}x{5}'.format(
                j_int, num_of_division, i_int, loss_val, i, j))

        q.task_done()
        print('{0} row finished'.format(i_int))
    return ()


def compute_mesh(model, trajectories, origin, data_x, data_y, optimizer, loss):
    """
        Compute all points of mesh for the 3D graph

      !WARNING!
      For a large path this will take a long time
      !WARNING!

      -----------------------------------------------------------------
      Parameters:
          model (models.Model):Model used for training
          trajectories (np.array):2 computed PCA components
          origin (np.array):Found minimum of the parameters
          data_x (np.array):Input data used in training
          data_y (np.array):Output data used in training
    """
    ################## Temporary variables ###############################################
    bound = GLOBAL_MAX_BOUND
    num_of_division = GLOBAL_DIV_NUM
    print('TOTAL CALCULATION SIZE')
    print(bound)
    print(num_of_division)
    ################## Temporary variables ###############################################

    global points
    points = np.empty((num_of_division, num_of_division))
    # import tf.keras
    processes = []
    print('Bounds: Min{0}, Max{1}. Num of divisions:{2}'.format(-GLOBAL_MAX_BOUND, GLOBAL_MAX_BOUND, num_of_division))
    print('Rows are in bound {0} to {1}'.format(ROW_START, ROW_END))
    print('Creating {0} processes...'.format(CPU_NUM))
    for i in range(0, CPU_NUM):
        # TODO !Hardcoded 1CPU work only!
        cln = model
        arges = [cln, origin, trajectories, data_x, data_y, bound, num_of_division, optimizer, loss]
        p = Thread(target=compute_one_loss, args=arges)
        p.start()
        processes.append(p)
    start = time.time()

    print('Creating thread....')
    i_s = np.linspace(-bound, bound, num=num_of_division)
    for ind in range(0, num_of_division):
        q.put((ind, i_s[ind]))

    print('Waiting for thread to finish...')
    q.join()

    # Send end signal
    for i in range(0, CPU_NUM):
        q.put((-1, 0.0))
    q.join()
    print('Returning....')
    print('Time taken:{0}'.format(time.time() - start))

    # Return variables
    res_points = np.array(points[:])
    res_points = res_points.reshape((num_of_division, num_of_division))
    return res_points, bound, num_of_division


def LossSurface(path_params, data_x: np.array, data_y: np.array, model: tf.keras.models.Model,
                optimizer, loss='MSE', division_number=500, global_max=250,
                max_CPU=32, row_start=-1, row_end=-1):
    """
      Compute and Visualise a function's loss surface

      -----------------------------------------------------------------
      Parameters:
          :param data_x: Input data used in training
          :param data_y: Output data used in training
          :param model (models.Model):Model used for training
          :param data_x:
          :param loss:
          :param path_params:
    """
    global GLOBAL_DIV_NUM
    GLOBAL_DIV_NUM = division_number
    global GLOBAL_MAX_BOUND
    GLOBAL_MAX_BOUND = global_max
    global CPU_NUM
    CPU_NUM = max_CPU

    global ROW_END
    if row_end == -1:
        ROW_END = int(2 * GLOBAL_MAX_BOUND)
    else:
        ROW_END = int(row_end)

    global ROW_START
    ROW_START = int(row_start)

    (traj, orig) = path_params
    print('[2]Computing Mesh for Loss Surface...')

    points, xy_b, num = compute_mesh(model=model,
                                     trajectories=traj,
                                     origin=orig,
                                     data_x=data_x,
                                     data_y=data_y,
                                     optimizer=optimizer, loss=loss)
    print('[3]Saving results...')
    result = {'points': points, 'xy_b': xy_b, 'num': num}

    fl = open('result_index{0}.txt'.format(row_start), 'wb')
    pickle.dump(result, fl)
    fl.close()
    print('FINISHED!')
