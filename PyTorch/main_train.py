from models.resnet_external import resnet
from models.resnoskip_external import  resnoskip

import torch
print(torch.cuda.device_count())
global cuda0
cuda0 = torch.device('cuda:0')

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import numpy as np

def to_categorical(y, num_classes):
    """ 1-hot encodes a tensor """
    return np.eye(num_classes, dtype='float32')[y]

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)
cats = to_categorical(trainset.targets, len(classes))
trainset.targets = torch.Tensor(cats)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=128,
                                          shuffle=True, num_workers=0)

testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform)
cats = to_categorical(testset.targets, len(classes))
testset.targets = torch.Tensor(cats)
testloader = torch.utils.data.DataLoader(testset, batch_size=128,
                                         shuffle=False, num_workers=0)
import torch.optim as optim

model = resnoskip(depth=110, num_classes=10)
criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=0.1, momentum=0.9, weight_decay=0.0005)
model = model.cuda()


import numpy as np
import torch.nn.functional as nnf
import pathlib
history_losses = []
history_accs = []
hist_val_accs = []
hist_val_loss = []
ptht = pathlib.Path().parent.absolute() / 'resnoskip110_trained_epochs=50'
f_name = str(ptht)
ptht2 = pathlib.Path().parent.absolute() / 'resnoskip110epoch_{0}'
sv_name = str(ptht2)
ptht3 = pathlib.Path().parent.absolute() / 'history_resnoskip110'
hs_name = str(ptht3)

f = open(sv_name.format(49), 'rb')
model.load_state_dict(torch.load(f))

for epoch in range(10):  # loop over the dataset multiple times
    running_loss = 0.0
    running_acc = 0.0
    val_loss = 0.0
    val_acc = 0.0
    counter = 0
    for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs = inputs.to(cuda0)
        labels = labels.to(cuda0)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = model(inputs)

        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        #Accuracy
        c = outputs.argmax(dim=1)
        #print(c)
        _, labels_tags = torch.max(labels, dim = 1) 
        #print(labels_tags)
        accuracy = (outputs.argmax(dim=1) == labels_tags).sum().float() / float( labels_tags.size(0) )
        #print('acc:{0}'.format(accuracy))

        # add statistics
        print('.', end='')
        running_loss += loss.item()
        running_acc += accuracy
        counter += 1
        #print('[{0}][{1}/128] Loss:{2:.8f}'.format(epoch+1, i+1, loss))
    # print statistics
    running_acc = running_acc / counter * 100.0
    
    print()
    print('[{0}] loss:{1:.8f}, acc:{2:.8f}'.format(epoch + 1, running_loss, running_acc))
    history_losses.append(running_loss)
    history_accs.append(running_acc)

    
    counter = 0
    for i, data in enumerate(testloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs = inputs.to(cuda0)
        labels = labels.to(cuda0)

        # zero the parameter gradients
        #optimizer.zero_grad()

        # forward + backward + optimize
        outputs = model(inputs)

        loss = criterion(outputs, labels)
        #loss.backward()
        #optimizer.step()

        #Accuracy
        c = outputs.argmax(dim=1)
        #print(c)
        _, labels_tags = torch.max(labels, dim = 1) 
        #print(labels_tags)
        accuracy = (outputs.argmax(dim=1) == labels_tags).sum().float() / float( labels_tags.size(0) )
        #print('acc:{0}'.format(accuracy))

        # add statistics
        print('.', end='')
        val_loss += loss.item()
        val_acc += accuracy
        counter += 1
        #print('[{0}][{1}/128] Loss:{2:.8f}'.format(epoch+1, i+1, loss))
    # print statistics
    val_acc = val_acc / counter * 100.0
    
    print()

    print('[{0}] val loss:{1:.8f}, val acc:{2:.8f}'.format(epoch + 1, val_loss, val_acc))
    hist_val_loss.append(val_loss)
    hist_val_accs.append(val_acc)
   

    #if epoch == 3:
    torch.save(model.state_dict(), sv_name.format(49 + epoch))
    
    print('### SAVED {0}###'.format(49 + epoch))
    
print('Finished Training')

print('Saving...')

torch.save(model.state_dict(), sv_name)
print(history_losses)
import pickle
f = open(hs_name, 'wb')
pickle.dump([history_losses, history_accs, hist_val_loss, hist_val_accs], f)









exit(0)
from visualisation.loss_surface import compute_mesh

import pickle
f = open('resnoskip110_rand_dirs', 'rb')
[rand_1, rand_2], orig = pickle.load(f)

traj = [rand_1, rand_2]
result = compute_mesh(model, traj, orig, trainloader, optimizer, criterion, cuda0, 0, 10)
f = open('pytorch_result_1', 'wb')
#result = [points, 1, 51]
import pickle
pickle.dump(result, f)
