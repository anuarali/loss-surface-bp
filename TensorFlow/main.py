import os
# Remove all warnings
import os.path as osp
import random

# set seed for better repeatability
from numpy.random import seed as set_seed
from tensorflow import random as trand
from tensorflow.keras.models import Model
from TensorFlow.models.dense_cifar import *
from TensorFlow.models.resnet50 import resnet50
from TensorFlow.models.taaf import taaf_dense

# import functions for visualisation
from TensorFlow.utils.loss_surface import *
from TensorFlow.utils.trajectory_math import *
from TensorFlow.utils.visualization import Visualization

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def load_taaf(n_hidden=3, n_neurons=6000, is_taaf=True):
    """
    Loads a TAAF or Linear network.
    ! GEO Dataset needs to be installed (Not in GIT)!

    :param n_hidden: number of hidden layers
    :param n_neurons: neurons per hidden layer
    :param is_taaf: True if TAAF, else Linear
    :return: (model, x, y, epoch, period, optimizer, loss, val_x, val_y)
    """

    seed = 42
    set_seed(seed)
    trand.set_seed(seed)
    random.seed = seed

    data_path = './data'
    fname_train_y = osp.join(data_path, 'ddf_geoser_tg_dataset_P-2_R-0_set-0_train.h5')
    fname_test_y = osp.join(data_path, 'ddf_geoser_tg_dataset_P-2_R-0_set-0_test.h5')

    fname_train_X = osp.join(data_path, 'ddf_geoser_lm_dataset_train.h5')
    fname_test_X = osp.join(data_path, 'ddf_geoser_lm_dataset_test.h5')

    n_epoch = 250  # number of epochs
    batch_size = 256
    l_rate = 5e-4

    # load the data
    ddf_train_y = pd.read_hdf(fname_train_y, 'table')
    ddf_test_y = pd.read_hdf(fname_test_y, 'table')

    ddf_train_X = pd.read_hdf(fname_train_X, 'table')
    ddf_test_X = pd.read_hdf(fname_test_X, 'table')

    # for checking the input dimensions
    in_size = ddf_train_X.shape[1]
    out_size = ddf_train_y.shape[1]
    print('Input size: {}  Output size: {}'.format(in_size, out_size))

    activation_function_name = 'sigmoid'
    dropout = 0.25  # 25 % of neurons won't be used each training epoch
    model_name = 'TAAF_3_6000'

    period = 4

    inputs = Input(shape=(in_size,), name='inputs')
    x = inputs
    for n in range(n_hidden):
        if is_taaf:
            x = taaf_dense(x, n_neurons, activation=activation_function_name, name='H' + str(n))
        else:
            x = x = Dense(n_neurons, activation=activation_function_name, name='H' + str(n))(x)
        x = Dropout(dropout, seed=seed)(x)

    # the TAAF allows the output layer to have a nonlinear activation as well
    if is_taaf:
        outputs = taaf_dense(x, out_size, activation=activation_function_name, name='outputs')
    else:
        outputs = Dense(out_size, activation='linear', name='outputs')(x)

    model = Model(inputs=inputs, outputs=outputs)
    optimizer = Nadam(lr=l_rate)
    loss = 'mean_absolute_error'
    model.compile(optimizer=optimizer, loss=loss, metrics=['acc'])

    return model, ddf_train_X, ddf_train_y, n_epoch, period, optimizer, loss, ddf_test_X, ddf_test_y


def load_cifar10(is_categorical=False):
    """
    Local version of TensorFlow dataset Cifar10:
        tf.keras.datasets.cifar10.load_data()

    >> installed locally in case tensorflow fails(it can)

    :param is_categorical: True if your model uses categories(otherwise single output expected)
    :return: cifar10 data in form of (x_train, y_train), (x_test, y_test)
    """
    import pathlib
    pt = pathlib.Path().parent.absolute() / 'TensorFlow' / 'data' / 'cifar10_tf_keras'
    strpt = str(pt)
    f = open(strpt, 'rb')
    _c = pickle.load(f)
    (train_im, train_lab), (test_im, test_lab) = _c
    train_im, test_im = train_im / 255.0, test_im / 255.0
    class_types = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog',
                   'frog', 'horse', 'ship', 'truck']
    if is_categorical is True:
        train_lab = tf.keras.utils.to_categorical(train_lab, len(class_types))
        test_lab = tf.keras.utils.to_categorical(test_lab, len(class_types))

    _c = (train_im, train_lab), (test_im, test_lab)
    return _c


def load_resnet50():
    """
    Loads a ResNet50 with following settings:

    Loss function - SparseCategoricalCrossEntropy(To combine single + multi categories)
    Optimizer  - Adam optimizer with learning rate decay(0.0001--)
    Metrics - Loss + Acc
    :return: model, loss_func, optimizer
    """
    initial_learning_rate = 0.0001
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate,
        decay_steps=100000,
        decay_rate=0.96,
        staircase=True)

    f_arr = np.zeros(10)
    f_train_im = np.zeros((1, 32, 32, 3))
    model = resnet50(f_train_im, f_arr)
    loss = 'sparse_categorical_crossentropy'
    optimizer = Adam(learning_rate=lr_schedule)
    model.compile(loss=loss, optimizer=optimizer, metrics=['acc'])
    return model, loss, optimizer


def train_model(model, cifar10=None, checkpoint_path=None, batch_size=128, history_file='model_training_history'):
    """
    Training a model with a checkpoint and history saving

    :param model: Compiled model
    :param cifar10: Dataset in form of (x, y, xt, yt)
    :param checkpoint_path: Path to checkpoint folder. Should be in form of .../'cp-{epoch:04d}.ckpt'
    :param batch_size: Default is 128
    :param history_file: Path to history file to save to
    :return: history of training
    """
    # Load data
    if cifar10 is None:
        cifar10 = load_cifar10()
    (train_im, train_lab), (test_im, test_lab) = cifar10

    # Set checkpoints to save model
    if checkpoint_path is None:
        import pathlib
        checkpoint_path = pathlib.Path().parent.absolute() / 'cp-{epoch:04d}.ckpt'
    mcc = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path, save_weights_only=True, save_freq='epoch')

    # Run training
    history = model.fit(train_im, train_lab, epochs=150, batch_size=128,
                        validation_data=(test_im, test_lab), callbacks=[mcc])

    # Save training history
    f = open(history_file, 'wb')
    pickle.dump(history, f)

    return history


if __name__ == '__main__':
    """
    Entry points of a project
    
    Needed libraries:
    TensorFlow, MatPlotLib, PyVista, Numpy, Pickle, VTK, Pandas, Sklearn
    
    ----------------------------------------------------------------------------------------------
    Structure:
    
    - data
        - Training: History and checkpoints of pre-trained models
        - Visualizations: Calculated loss surface in form of (data[size][size], scale, resolution)
        cifar10_tf_keras: Local version of [tf.keras.datasets.cifar10] downloaded
        
    - models
        resnet50:         ResNet's original architecture based on ImageNet dataset
        resnet50_noskip:  ResNet50 with no skip connections used
        resnet56:         ResNet's lighter version designed for Cifar10
        resnet56_noskip:  ResNet56 with no skip connections used
        densenet:         DenseNet121 based on ImageNet dataset (NOT USED)
        dense_cifar:      DenseNet reworked for Cifar10 dataset
        
    - utils
        loss_surface:     Calculation of the Loss surface in Tensorflow framework(1 GPU+CPU per launch). 
        trajectory_math:  Pre-calculations for Loss surface (PCA, random trajectories, load checks)
        visualization:    Methods of loss surface visualization (MatplotLib, VTK, ...)
    """
    import sys
    print(sys.version)

    name = 'resnet50_noskip_pca'
    Visualization.print_previsualized()
    Visualization.visualize_plot(model_name=name, vis_method=Visualization.Method.VTK_image, xy_scale=1,
                                 save_file='', divide_plot=1)
