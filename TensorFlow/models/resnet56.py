import tensorflow as tf

'''
Custom Re-Implementation of PyTorch ResNet from:
https://github.com/bearpaw/pytorch-classification/blob/master/models/cifar/resnet.py
Uses TensorFlow 2
'''


class ResidualLayer(tf.keras.Model):
    def __init__(self, planes, layer_index, block_index, stride=1, downsample=None):
        if downsample is None:
            nm = 'stride{0}'.format(stride)
        else:
            nm = 'stride{0}_DownSample'.format(stride)
        super(ResidualLayer, self).__init__(name='r56__RES{0}_{1}_{2}'.format(layer_index, block_index, nm))
        # Conv with stride
        self.conv1 = tf.keras.layers.Conv2D(planes, kernel_size=(3, 3), strides=stride, use_bias=False, padding='same',
                                            name='r56___RES{0}_{1}___Conv2D_3x3_1'.format(layer_index, block_index))
        self.bn1 = tf.keras.layers.BatchNormalization(axis=1, center=True, scale=True,
                                                      name='r56___RES{0}_{1}___BatchNorm1'.format(layer_index,
                                                                                                  block_index))
        self.relu = tf.keras.layers.ReLU(name='r56___RES{0}_{1}___ReLU1'.format(layer_index, block_index))
        # Conv without stride
        self.conv2 = tf.keras.layers.Conv2D(planes, kernel_size=(3, 3), strides=1, use_bias=False, padding='same',
                                            name='r56___RES{0}_{1}___Conv2D_3x3_2'.format(layer_index, block_index))
        self.bn2 = tf.keras.layers.BatchNormalization(axis=1, center=True, scale=True,
                                                      name='r56___RES{0}_{1}___BatchNorm2'.format(layer_index,
                                                                                                  block_index))

        self.downsample = downsample
        self.stride = stride

    def call(self, inputs, training=False):
        residual = inputs  # save residual
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)

        x = self.relu(x)
        x = self.conv2(x)
        x = self.bn2(x, training=training)
        # Downsample
        if self.downsample is not None:  # If downsample -> decrease it
            residual = self.downsample[0](inputs)
            residual = self.downsample[1](residual, training=training)
        x += residual
        x = self.relu(x)
        return x


class ResNet56(tf.keras.Model):

    def __init__(self, input_shape=(32, 32, 1)):
        super(ResNet56, self).__init__()
        self.input_layer = tf.keras.layers.Input(input_shape)  # SOME TENSOR MAGIC
        # CONV1 STEP ----------------------------------------------------------------------
        self.zeropad1 = tf.keras.layers.ZeroPadding2D(padding=(1, 1),
                                                      name='r56__CONV1___zero_padding')  # +2 in Width/Height
        self.conv1 = tf.keras.layers.Conv2D(16, kernel_size=(3, 3), strides=(1, 1),
                                            name='r56__CONV1___Conv2D', padding='valid')  # 'valid' = -2 in Width/Height
        self.bn1 = tf.keras.layers.BatchNormalization(axis=1, center=True, scale=True, name='r56__CONV1___BatchNorm')
        self.relu1 = tf.keras.layers.ReLU(name='r56__CONV1___ReLU')
        # STAGE 1 (NO DOWNSAMPLE) ----------------------------------------------------------------------
        layer = ResidualLayer(16, layer_index=1, block_index=1, downsample=None)
        setattr(self, 'layer1_1', layer)
        for i in range(2, 10):
            layer = ResidualLayer(16, layer_index=1, block_index=i)
            setattr(self, 'layer1_{0}'.format(i), layer)
        # STAGE 2 (DOWNSAMPLE)  ----------------------------------------------------------------------
        downsample2 = [tf.keras.layers.Conv2D(32, padding='same',
                                              kernel_size=1, strides=2, use_bias=False),
                       tf.keras.layers.BatchNormalization()]

        layer = ResidualLayer(32, layer_index=2, block_index=1, downsample=downsample2, stride=2)
        setattr(self, 'layer2_1', layer)
        for i in range(2, 10):
            layer = ResidualLayer(32, layer_index=2, block_index=i)
            setattr(self, 'layer2_{0}'.format(i), layer)
        # STAGE 3 (DOWNSAMPLE) ----------------------------------------------------------------------
        downsample3 = [tf.keras.layers.Conv2D(64, padding='same',
                                              kernel_size=1, strides=2, use_bias=False),
                       tf.keras.layers.BatchNormalization()]

        layer = ResidualLayer(64, layer_index=3, block_index=1, downsample=downsample3, stride=2)
        setattr(self, 'layer3_1', layer)
        for i in range(2, 10):
            layer = ResidualLayer(64, layer_index=3, block_index=i)
            setattr(self, 'layer3_{0}'.format(i), layer)
        # FINAL STAGE (POOLING, FC, SOFTMAX) --------------------------------------------------------
        self.avgPool = tf.keras.layers.AveragePooling2D(8, name='r56__FINAL__AvgPool2D')
        self.flatten = tf.keras.layers.Flatten()
        self.fc = tf.keras.layers.Dense(10, use_bias=True, name='r56__FINAL__Dense_FC')  #
        self.softmax = tf.keras.layers.Softmax(name='r56__FINAL__Softmax')
        self.out = self.call(self.input_layer)  # SOME TENSOR MAGIC

    def call(self, inputs, training=False):
        # CONV1
        x = self.zeropad1(inputs)
        x = self.conv1(x)
        x = self.bn1(x, training=training)
        x = self.relu1(x)
        # LAYER 1
        for i in range(1, 10):
            layer = getattr(self, 'layer1_{0}'.format(i))
            x = layer(x, training)
        # LAYER 2
        for i in range(1, 10):
            layer = getattr(self, 'layer2_{0}'.format(i))
            x = layer(x, training)
        # LAYER 3
        for i in range(1, 10):
            layer = getattr(self, 'layer3_{0}'.format(i))
            x = layer(x)
        # POOLING AND SOFTMAXING
        x = self.avgPool(x)
        x = self.flatten(x)
        x = self.fc(x)
        x = self.softmax(x)
        return x
