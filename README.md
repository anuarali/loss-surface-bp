# Loss surface visualization

Code of the bachelor thesis _"Neural network learning visualization"_.

Author: _Alikhan Anuarbekov_

Supervisor: _Vladimír Kunc_

External libraries used:
* TensorFlow-Keras 2
* MatPlotLib
* PyVista
* Numpy
* Pickle
* VTK
* Pandas
* Sklearn
