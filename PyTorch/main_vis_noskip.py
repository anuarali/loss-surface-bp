
import sys
args = sys.argv
row_start = 0
row_end = 200
if len(args) < 3:
    print('wrong number of arguments!')
    exit(1)

    #global_max = int(args[1])
row_start = int(args[1]) 
row_end = int(args[2])
    #cpu_num = int(args[4])
    #num_of_divs = int(args[5])


from models.resnet_external import resnet
from models.resnoskip_external import  resnoskip


import torch
print(torch.cuda.device_count())
global cuda0
cuda0 = torch.device('cuda:0')

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import numpy as np

def to_categorical(y, num_classes):
    """ 1-hot encodes a tensor """
    return np.eye(num_classes, dtype='float32')[y]

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)
cats = to_categorical(trainset.targets, len(classes))
trainset.targets = torch.Tensor(cats)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=128,
                                          shuffle=True, num_workers=0)

testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform)
cats = to_categorical(testset.targets, len(classes))
testset.targets = torch.Tensor(cats)
testloader = torch.utils.data.DataLoader(testset, batch_size=128,
                                         shuffle=False, num_workers=0)
import torch.optim as optim

model = resnoskip(depth=110, num_classes=10)
criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=0.1, momentum=0.9, weight_decay=0.0005)
model = model.cuda()


import numpy as np
import torch.nn.functional as nnf
import pathlib
history_losses = []
history_accs = []
ptht = pathlib.Path().parent.absolute() / 'resnoskipRandDirs'
f_name = str(ptht)
ptht2 = pathlib.Path().parent.absolute() / 'resnoskip110epoch_{0}'
sv_name = str(ptht2)
ptht3 = pathlib.Path().parent.absolute() / 'history'
hs_name = str(ptht2)

#f = open(sv_name.format(35), 'rb')
#model.load_state_dict(torch.load(f))



from visualisation.loss_surface import compute_mesh

import pickle
f = open(f_name, 'rb')
[rand_1, rand_2], orig = pickle.load(f)

traj = [rand_1, rand_2]
result = compute_mesh(model, traj, orig, trainloader, optimizer, criterion, cuda0, row_start, row_end)
f = open('noskip_{0}_{1}'.format(row_start, row_end), 'wb')
#result = [points, 1, 51]
import pickle
pickle.dump(result, f)
